import javax.swing.*; // wczytanie biblioteki graficznej swing
import java.awt.*; // wczytanie biblioteki graficznej awt
import java.awt.event.*;

public class Kalkulator
{
  
  public static void main(String args[])
  {
    Okno okno=new Okno(); // tworzymy nowy obiekt typu Okno (dalej b?dziemy o nim m�wi? "okienko"), kt�ry zaraz stworzymy
    okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // zamkni?cie okienka ma by? jednoznaczne z zamkni?ciem programu
    okno.setVisible(true); // nasze okienko ma by? widoczne
  }
}  

class Okno extends JFrame // tworzymy nowy obiekt "Okno"
{
  public Okno() // "konstruktor" czyli g?�wna funkcja danej klasy o takiej samej nazwie co klasa. Okre?la w?a?ciwo?ci okienka
  {
    setTitle("Kalkulator"); // Nadajemy nazw? okienka (b?dzie si? wy?wietla? na niebieskim pasku programu)
    setSize(160,250); // Okre?lamy wymiary okienka w pikselach (160 szeroko?ci, 250 wysoko?ci)
    
    
    MojPanel p=new MojPanel(); // Tworzymy nowy obiekt typu "M�jPanel" (dalej nazywamy go "panel"), kt�ry zaraz stworzymy
    Container powzaw=getContentPane(); // tworzymy obiekt "powzaw" kt�ry stanowi powierzchni? okienka. 
    powzaw.add(p); // do tej powierzchni dodajemy panel, kt�ry stworzyli?my dwie linijki wy?ej
  }
}
  
class MojPanel extends JPanel // tworzymy nowy obiekt M�jPanel
{
  
  JTextField a;     // definiujemy zmienn? "a" typu "pole tekstowe" dla pierwszej liczby
  JTextField b;     // definiujemy zmienn? "b" typu "pole tekstowe" dla drugiej liczby
  JTextField wynik; // definiujemy zmienn? "wynik" typu "pole tekstowe" dla wyniku
  
  // Zmienne te definiujemy bezpo?rednio w klasie, ?eby by?y dost?pne dla wszystkich funkcji w tej klasie
  
  public MojPanel() // "konstruktor" czyli g?�wna funkcja danej klasy o takiej samej nazwie co klasa. Tu tworzymy fizycznie nasz "panel"
  {
    setLayout(new FlowLayout(FlowLayout.LEFT)); // okre?lamy uk?ad element�w w panelu. 
                                                // Tu: FlowLayout, czyli wszystkie elementy s? wyr�wnane do lewej strony
                                                // ka?dy kolejny element b?dzie "doklejany" do poprzedniego z prawej strony

  
    JLabel lab1=new JLabel("Liczba a");    // Tworzymy etykietk? tekstow? "lab1"
    add(lab1);                             // i dodajemy j? do naszego panelu
    
    a=new JTextField("",12);               // Tworzymy nowe pole tekstowe przypisane do zdefiniowanej wcze?niej zmiennej "a"
    add(a);                                // i dodajemy go do naszego panelu
    
    JLabel lab2=new JLabel("Liczba b");    // Tworzymy etykietk? tekstow? "lab2"
    add(lab2);                             // i dodajemy j? do naszego panelu
    
    b=new JTextField("",12);               // Tworzymy nowe pole tekstowe przypisane do zdefiniowanej wcze?niej zmiennej "b"
    add(b);                                // i dodajemy go do naszego panelu
        
    
    JButton plus=new JButton("+");        // Tworzymy przycisk " + " o nazwie "plus"
    add(plus);                            // i dodajemy go do naszego panelu
    
    JButton minus=new JButton("-");       // Tworzymy przycisk " - " o nazwie "minus"
    add(minus);                           // i dodajemy go do naszego panelu
    
    JButton mnozenie=new JButton("*");    // Tworzymy przycisk " * " o nazwie "mnozenie"
    add(mnozenie);                        // i dodajemy go do naszego panelu
    
    JButton dzielenie=new JButton("/");   // Tworzymy przycisk " / " o nazwie "dzielenie"
    add(dzielenie);                       // i dodajemy go do naszego panelu
    
    // Mamy ju? elementy kalkulatora, teraz musimy stworzy? co?, co b?dzie ci?gle monitorowa?o
    // co te? si? dzieje z naszymi przyciskami. Do ka?dego z nich stworzymy wi?c specjalny "pods?uch"
    
    ActionListener sl1=new Dodaj();       // Tworzymy "pods?uch" o nazwie "sl1" w postaci funkcji Dodaj()
    plus.addActionListener(sl1);          // i podpinamy go pod przycisk o nazwie "plus"
    
    ActionListener sl2=new Odejmij();     // Tworzymy "pods?uch" o nazwie "sl2" w postaci funkcji Odejmij()
    minus.addActionListener(sl2);         // i podpinamy go pod przycisk o nazwie "minus" 
    
    ActionListener sl3=new Pomnoz();      // Tworzymy "pods?uch" o nazwie "sl3" w postaci funkcji Pomnoz()
    mnozenie.addActionListener(sl3);      // i podpinamy go pod przycisk o nazwie "mnozenie"  
    
    ActionListener sl4=new Podziel();     // Tworzymy "pods?uch" o nazwie "sl4" w postaci funkcji Podziel()
    dzielenie.addActionListener(sl4);     // i podpinamy go pod przycisk o nazwie "dzielenie" 
    
    
    JLabel lab3=new JLabel("wynik");      // Tworzymy etykietk? tekstow? "lab1"
    add(lab3);                            // i dodajemy j? do naszego panelu
    
    wynik=new JTextField("",12);          // Tworzymy nowe pole tekstowe przypisane do zdefiniowanej wcze?niej zmiennej "wynik"
    add(wynik);                           // i dodajemy go do naszego panelu
    
    }
  
  class Dodaj implements ActionListener   
  {
    public void actionPerformed(ActionEvent zdarzenie) // wykryj zdarzenie - klikni?cie
    {
      long n=Integer.parseInt(a.getText()); // Pobierz zawarto?? pola tekstowego "a", zamie? na liczb? typu "long" i przypisz do zmiennej "n"
      long m=Integer.parseInt(b.getText()); // Pobierz zawarto?? pola tekstowego "b", zamie? na liczb? typu "long" i przypisz do zmiennej "m"
      long suma=n+m; // wykonaj dzia?anie
      wynik.setText(""+suma+" "); // podstaw warto?? do pola tekstowego "wynik"
    }
  }
  
    class Odejmij implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) // wykryj zdarzenie - klikni?cie
    {
      long n=Integer.parseInt(a.getText()); // Pobierz zawarto?? pola tekstowego "a", zamie? na liczb? typu "long" i przypisz do zmiennej "n"
      long m=Integer.parseInt(b.getText()); // Pobierz zawarto?? pola tekstowego "b", zamie? na liczb? typu "long" i przypisz do zmiennej "m"
      long roznica=n-m; // wykonaj dzia?anie
      wynik.setText(""+roznica); // podstaw warto?? do pola tekstowego "wynik"
    }
  }
    
  class Pomnoz implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) // wykryj zdarzenie - klikni?cie
    {
      long n=Integer.parseInt(a.getText()); // Pobierz zawarto?? pola tekstowego "a", zamie? na liczb? typu "long" i przypisz do zmiennej "n"
      long m=Integer.parseInt(b.getText()); // Pobierz zawarto?? pola tekstowego "b", zamie? na liczb? typu "long" i przypisz do zmiennej "m"
      long iloczyn=n*m; // wykonaj dzia?anie
      wynik.setText(""+iloczyn); // podstaw warto?? do pola tekstowego "wynik"
    }
  }
      
  class Podziel implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) // wykryj zdarzenie - klikni?cie
    {
      long n=Integer.parseInt(a.getText()); // Pobierz zawarto?? pola tekstowego "a", zamie? na liczb? typu "long" i przypisz do zmiennej "n"
      long m=Integer.parseInt(b.getText()); // Pobierz zawarto?? pola tekstowego "b", zamie? na liczb? typu "long" i przypisz do zmiennej "m"
      float iloraz=(float)n/m; // wykonaj dzia?anie. Zwr�? uwag? na typ "float". (float) przed dzia?aniem zamienia warto?? na liczb? zmiennoprzecinkow?
                               // przez co poprawnie wy?wietla u?amek
      wynik.setText(""+iloraz); // podstaw warto?? do pola tekstowego "wynik"
    }
  }
  
  
  
  
  
  
}
